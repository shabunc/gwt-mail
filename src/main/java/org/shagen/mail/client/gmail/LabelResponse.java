package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

public interface LabelResponse extends JsonBuilder {
    String getName();
    String getId();
    String getMessagesUnread();
    String getMessagesTotal();
}
