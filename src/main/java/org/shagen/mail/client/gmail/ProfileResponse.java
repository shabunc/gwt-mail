package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

public interface ProfileResponse extends JsonBuilder{
    String getDisplayName();
    ProfileImageResponse getImage();
}
