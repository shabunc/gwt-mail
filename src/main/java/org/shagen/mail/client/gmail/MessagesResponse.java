package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

import java.util.List;

public interface MessagesResponse extends JsonBuilder {
    List<MessageResponse> getItems();
}
