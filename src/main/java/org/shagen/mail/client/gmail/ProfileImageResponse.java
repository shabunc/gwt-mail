package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

public interface ProfileImageResponse extends JsonBuilder{
    String getUrl();
}
