package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

public interface FullMessageResponse extends JsonBuilder {

    String getHtml();
}
