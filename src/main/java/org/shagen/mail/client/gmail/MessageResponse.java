package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

public interface MessageResponse extends JsonBuilder {
    String getId();
    String getFrom();
    String getSubject();
    String getPersonalName();
    String getAddress();
    Boolean getIsUnread();
}
