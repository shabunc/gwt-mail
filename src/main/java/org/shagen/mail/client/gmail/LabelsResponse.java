package org.shagen.mail.client.gmail;

import com.google.gwt.query.client.builders.JsonBuilder;

import java.util.List;

public interface LabelsResponse extends JsonBuilder {
    List<LabelResponse> getItems();
}
