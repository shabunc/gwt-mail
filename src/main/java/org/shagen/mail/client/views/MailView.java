package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class MailView extends BaseWithElement {

    private static final MailViewUiBinder uiBinder = GWT.create(MailViewUiBinder.class);

    @UiField
    public IFrameElement htmlBodyContainer;

    @UiField
    public Element controls;

    public MailView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/MailView.ui.xml")
    interface MailViewUiBinder extends UiBinder<DivElement, MailView> {}
}
