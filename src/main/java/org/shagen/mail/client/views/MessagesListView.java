package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class MessagesListView  extends BaseWithElement  {

    private static final MessagesListViewUiBinder uiBinder = GWT.create(MessagesListViewUiBinder.class);

    @UiField
    public Element messages;

    @UiField
    public Element progressBar;

    public MessagesListView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/MessagesListView.ui.xml")
    interface MessagesListViewUiBinder extends UiBinder<Element, MessagesListView> {}
}
