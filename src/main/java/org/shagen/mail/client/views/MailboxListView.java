package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class MailboxListView extends BaseWithElement {

    private static final MailboxListViewUiBinder uiBinder = GWT.create(MailboxListViewUiBinder.class);

    @UiField
    public Element mailboxes;

    public MailboxListView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/MailboxListView.ui.xml")
    interface MailboxListViewUiBinder extends UiBinder<Element, MailboxListView> {}
}
