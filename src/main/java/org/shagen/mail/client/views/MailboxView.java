package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class MailboxView extends BaseWithElement {

    private static final MailboxViewUiBinder uiBinder = GWT.create(MailboxViewUiBinder.class);

    @UiField
    public Element item;

    @UiField
    public Element name;

    @UiField
    public Element totalCount;

    @UiField
    public Element unreadCount;

    public MailboxView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/MailboxView.ui.xml")
    interface MailboxViewUiBinder extends UiBinder<Element, MailboxView> {}
}
