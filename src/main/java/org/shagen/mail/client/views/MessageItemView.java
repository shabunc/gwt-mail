package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class MessageItemView extends BaseWithElement {

    private static final MessageItemViewUiBinder uiBinder = GWT.create(MessageItemViewUiBinder.class);

    @UiField
    public Element subject;

    @UiField
    public Element sender;

    public interface Stylesheet extends CssResource {
        String isUnread();
    }

    @UiField
    public Stylesheet stylesheet;

    public MessageItemView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/MessageItemView.ui.xml")
    interface MessageItemViewUiBinder extends UiBinder<AnchorElement, MessageItemView> {}
}


