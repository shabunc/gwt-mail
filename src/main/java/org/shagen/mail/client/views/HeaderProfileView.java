package org.shagen.mail.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Image;
import jetbrains.jetpad.mapper.gwt.BaseWithElement;

public class HeaderProfileView extends BaseWithElement {

    private static final HeaderProfileViewUiBinder uiBinder = GWT.create(HeaderProfileViewUiBinder.class);

    @UiField
    public Element userName;

    @UiField
    public ImageElement userPic;

    public HeaderProfileView() {
        setElement(uiBinder.createAndBindUi(this));
    }

    @UiTemplate("bind/HeaderProfileView.ui.xml")
    interface HeaderProfileViewUiBinder extends UiBinder<Element, HeaderProfileView> {}
}
