package org.shagen.mail.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.impl.AsyncFragmentLoader;
import com.google.gwt.dom.client.Element;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.plugins.ajax.Ajax;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.gwt.WithElement;
import org.shagen.mail.client.gmail.*;
import org.shagen.mail.client.mappers.HeaderProfileMapper;
import org.shagen.mail.client.mappers.MailMapper;
import org.shagen.mail.client.mappers.MailboxListMapper;
import org.shagen.mail.client.mappers.MessagesListMapper;
import org.shagen.mail.client.models.*;
import org.shagen.mail.client.models.Mail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.google.gwt.query.client.GQuery.$;


public class Mail2 implements EntryPoint {
    Logger logger = Logger.getLogger("Mail App");
    private Mapper<MessagesList, ? extends WithElement> messagesListMapper;

    @UiTemplate("templates/Mail2.ui.xml")
    interface Binder extends UiBinder<HTML, Mail2> {
    }

    private static final Binder binder = GWT.create(Binder.class);

    private MessagesList model = createModel();

            private Map<String, Mailbox> mailboxesMap = new HashMap<>();

    public void onModuleLoad() {
        HTML outer = binder.createAndBindUi(this);

        // displayed.
        RootLayoutPanel root = RootLayoutPanel.get();
        root.add(outer);

        messagesListMapper = new MessagesListMapper(model);
        messagesListMapper.attachRoot();

        Element messagesListTargetElement = messagesListMapper.getTarget().getElement();
        $("#messagesList").append(messagesListTargetElement);

        Mapper<Mail, ? extends WithElement> mailMapper = new MailMapper(new Mail());
        mailMapper.attachRoot();

        $("#mailContainer").append(mailMapper.getTarget().getElement());

        Mapper<MailboxList, ? extends WithElement> mailboxListMapper = new MailboxListMapper(createMailbox());
        mailboxListMapper.attachRoot();

        Element mailboxList = mailboxListMapper.getTarget().getElement();
        $("#mailboxContainer").append(mailboxList);


        requestAndUpdateTheModel(mailboxListMapper.getSource().mailboxes.get(0).id.get());
        initEvents();
        refreshMailboxData();
        updateProfileData();
    }

    private void updateProfileData() {

        Ajax.get("/mail/profile", Ajax.createSettings()).done(new Function() {
            @Override
            public void f() {
                ProfileResponse profileResponse = GWT.<ProfileResponse>create(ProfileResponse.class).load(arguments(0));

                HeaderProfileMapper headerProfileMapper = new HeaderProfileMapper(new HeaderProfile(profileResponse));
                headerProfileMapper.attachRoot();

                $("#appHeader").append(headerProfileMapper.getTarget().getElement());
            }
        });
    }

    private void initEvents() {
        $(RootLayoutPanel.get().getElement()).on("MAILBOX_CHANGED", new Function() {
            @Override
            public void f() {
                Mailbox mailbox = arguments(0);
                requestAndUpdateTheModel(mailbox.id.get());
            }
        });
    }

    private MailboxList createMailbox() {
        MailboxList mailboxList = new MailboxList();
        Mailbox inboxMailbox = new Mailbox("INBOX", "INBOX");
        mailboxList.mailboxes.add(inboxMailbox);

        Mailbox sentMailbox = new Mailbox("SENT", "SENT");
        mailboxList.mailboxes.add(sentMailbox);

        mailboxesMap.put(inboxMailbox.id.get(), inboxMailbox);
        mailboxesMap.put(sentMailbox.id.get(), sentMailbox);

        return mailboxList;
    }

    private void requestAndUpdateTheModel(String mailboxId) {
        Ajax.Settings settings = Ajax.createSettings();
        model.items.clear();

        messagesListMapper.getSource().isLoaded.set(false);
        Ajax.get("/mail/messages/" + mailboxId, settings).done(new Function() {
            @Override
            public void f() {
                messagesListMapper.getSource().isLoaded.set(true);
                    
                MessagesResponse messagesResponse = GWT.<MessagesResponse>create(MessagesResponse.class).load(arguments(0));

                for (MessageResponse messageResponse : messagesResponse.getItems()) {
                    model.items.add(createMessageItem(messageResponse));
                    logger.log(Level.INFO, "MESSAGE:" + messagesResponse.toString());
                }
            }
        });
    }

    private void refreshMailboxData() {
        Ajax.get("/mail/mailboxes", Ajax.createSettings()).done(new Function() {
            @Override
            public void f() {
                LabelsResponse labelsResponse = GWT.<LabelsResponse>create(LabelsResponse.class).load(arguments(0));

                for (LabelResponse label: labelsResponse.getItems()) {
                    if (mailboxesMap.containsKey(label.getId())) {
                        Mailbox mailbox = mailboxesMap.get(label.getId());
                        mailbox.totalMessages.set(label.getMessagesTotal());
                        mailbox.unreadMessages.set(label.getMessagesUnread());
                    }
                }
            }
        });
    }

    private MessagesList createModel() {
        MessagesList result = new MessagesList();
        return result;
    }

    private MessageItem createMessageItem(MessageResponse response) {
        return createMessageItem(response.getId(),
                response.getSubject(), response.getPersonalName(), response.getIsUnread());
    }

    private MessageItem createMessageItem(String id, String subject, String from, Boolean isUnread) {
        MessageItem result = new MessageItem();
        result.id.set(id);
        result.subject.set(subject);
        result.sender.set(from);
        result.isUnread.set(isUnread);

        return result;
    }

}
