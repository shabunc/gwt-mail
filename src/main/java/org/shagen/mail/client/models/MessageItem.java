package org.shagen.mail.client.models;

import jetbrains.jetpad.model.children.SimpleComposite;
import jetbrains.jetpad.model.property.Property;
import jetbrains.jetpad.model.property.ValueProperty;

public class MessageItem extends SimpleComposite<MessagesList, MessageItem> {
    public final Property<String> id = new ValueProperty<>("id");
    public final Property<String> subject = new ValueProperty<>("Subject");
    public final Property<String> sender = new ValueProperty<>("From");
    public final Property<Boolean> isUnread = new ValueProperty<>(false);
}


