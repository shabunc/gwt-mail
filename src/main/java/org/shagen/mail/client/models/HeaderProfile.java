package org.shagen.mail.client.models;

import jetbrains.jetpad.model.property.Property;
import jetbrains.jetpad.model.property.ValueProperty;
import org.shagen.mail.client.gmail.ProfileResponse;

public class HeaderProfile {
    public final Property<String> userName = new ValueProperty<>("PROFILE");
    public final Property<String> userPic = new ValueProperty<>("");

    public HeaderProfile(ProfileResponse profileResponse) {
        this.userName.set(profileResponse.getDisplayName());
        this.userPic.set(profileResponse.getImage().getUrl());
    }
}
