package org.shagen.mail.client.models;

import jetbrains.jetpad.model.property.Property;
import jetbrains.jetpad.model.property.ValueProperty;

public class Mail {
    public final Property<String> htmlBody = new ValueProperty<>("<h1>HERE WE CAN PLACE SOME ADS :)</h1>");
    public final Property<Boolean> isInactive = new ValueProperty<>(true);
}
