package org.shagen.mail.client.models;

import jetbrains.jetpad.model.children.ChildList;
import jetbrains.jetpad.model.children.SimpleComposite;
import jetbrains.jetpad.model.collections.list.ObservableList;

public class MailboxList {
    public final ObservableList<Mailbox> mailboxes = new ChildList<>(this);
}
