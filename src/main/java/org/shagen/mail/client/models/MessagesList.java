package org.shagen.mail.client.models;

import jetbrains.jetpad.model.children.ChildList;
import jetbrains.jetpad.model.children.SimpleComposite;
import jetbrains.jetpad.model.collections.list.ObservableList;
import jetbrains.jetpad.model.property.Property;
import jetbrains.jetpad.model.property.ValueProperty;

public class MessagesList {
    public final ObservableList<MessageItem> items = new ChildList<>(this);
    public final Property<Boolean> isLoaded = new ValueProperty<>(false);
}



