package org.shagen.mail.client.models;

import jetbrains.jetpad.model.children.SimpleComposite;
import jetbrains.jetpad.model.property.Property;
import jetbrains.jetpad.model.property.ValueProperty;
import org.shagen.mail.client.gmail.LabelResponse;

public class Mailbox extends SimpleComposite<MailboxList, Mailbox> {
    public Property<String> id =  new ValueProperty<>("__ID__");
    public Property<String> name =  new ValueProperty<>("__NAME__");
    public Property<String> totalMessages =  new ValueProperty<>("~");
    public Property<String> unreadMessages =  new ValueProperty<>("~");

    public Mailbox(String id, String name) {
        this.id.set(name);
        this.name.set(name);
    }

    public Mailbox(LabelResponse mailBox) {
        this.id.set(mailBox.getId());
        this.name.set(mailBox.getName());
        this.totalMessages.set(mailBox.getMessagesTotal());
        this.unreadMessages.set(mailBox.getMessagesUnread());
    }

}
