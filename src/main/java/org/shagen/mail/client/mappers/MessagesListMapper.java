package org.shagen.mail.client.mappers;

import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.MapperFactory;
import jetbrains.jetpad.mapper.Synchronizers;
import jetbrains.jetpad.mapper.gwt.WithElement;
import org.shagen.mail.client.models.MessageItem;
import org.shagen.mail.client.models.MessagesList;
import org.shagen.mail.client.views.MessagesListView;

import static jetbrains.jetpad.mapper.Synchronizers.forObservableRole;
import static jetbrains.jetpad.mapper.gwt.DomUtil.hasAttribute;
import static jetbrains.jetpad.mapper.gwt.DomUtil.withElementChildren;

public class MessagesListMapper extends Mapper<MessagesList, MessagesListView> {

    public MessagesListMapper(MessagesList source) {
        super(source, new MessagesListView());
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        // unfiltered list
        conf.add(forObservableRole(this, getSource().items, withElementChildren(getTarget().messages),
                new MapperFactory<MessageItem, WithElement>() {
                    @Override
                    public Mapper<? extends MessageItem, ? extends WithElement> createMapper(MessageItem source) {
                        return new MessageItemMapper(source);
                    }
                }));

        conf.add(Synchronizers.forPropsOneWay(getSource().isLoaded, hasAttribute(getTarget().progressBar, "hidden", "hidden")));

    }
}
