package org.shagen.mail.client.mappers;

import com.google.gwt.core.client.GWT;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.plugins.ajax.Ajax;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.Synchronizers;
import org.shagen.mail.client.gmail.FullMessageResponse;
import org.shagen.mail.client.gmail.MessageResponse;
import org.shagen.mail.client.gmail.MessagesResponse;
import org.shagen.mail.client.models.Mailbox;
import org.shagen.mail.client.models.MessageItem;
import org.shagen.mail.client.views.MailboxView;
import org.shagen.mail.client.views.MessageItemView;

import static com.google.gwt.query.client.GQuery.$;
import static jetbrains.jetpad.mapper.gwt.DomUtil.editableTextOf;
import static jetbrains.jetpad.mapper.gwt.DomUtil.innerTextOf;

public class MailboxMapper extends Mapper<Mailbox, MailboxView> {
    public  MailboxMapper(final Mailbox source) {
        super(source, new MailboxView());

        $(getTarget().getElement()).click(new Function() {
            @Override
            public boolean f(Event e) {
                $(RootLayoutPanel.get().getElement()).trigger("MAILBOX_CHANGED", source);
                return true;
            }
        });
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        conf.add(Synchronizers.forPropsOneWay(getSource().name, innerTextOf(getTarget().name)));
        conf.add(Synchronizers.forPropsOneWay(getSource().totalMessages, innerTextOf(getTarget().totalCount)));
        conf.add(Synchronizers.forPropsOneWay(getSource().unreadMessages, innerTextOf(getTarget().unreadCount)));
    }
}
