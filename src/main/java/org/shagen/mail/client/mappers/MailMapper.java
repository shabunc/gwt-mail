package org.shagen.mail.client.mappers;

import com.google.gwt.core.client.GWT;
import com.google.gwt.query.client.Function;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.Synchronizers;
import org.shagen.mail.client.gmail.FullMessageResponse;
import org.shagen.mail.client.models.Mail;
import org.shagen.mail.client.models.MessageItem;
import org.shagen.mail.client.views.MailView;

import static com.google.gwt.query.client.GQuery.$;
import static jetbrains.jetpad.mapper.gwt.DomUtil.attribute;
import static jetbrains.jetpad.mapper.gwt.DomUtil.hasAttribute;
import static jetbrains.jetpad.mapper.gwt.DomUtil.innerTextOf;

public class MailMapper extends Mapper<Mail, MailView> {
    public MailMapper(final Mail source) {
        super(source, new MailView());

        $(RootLayoutPanel.get().getElement()).on("MESSAGE_LOADED", new Function() {
            @Override
            public void f() {
                //MessageItem data = GWT.<MessageItem>create(MessageItem.class).load(arguments(0));
                FullMessageResponse data = (FullMessageResponse) arguments(0);


                source.htmlBody.set(data.getHtml());
                source.isInactive.set(false)    ;
            }
        });
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        conf.add(Synchronizers.forPropsOneWay(getSource().htmlBody, attribute(getTarget().htmlBodyContainer, "srcdoc")));
        conf.add(Synchronizers.forPropsOneWay(getSource().isInactive, hasAttribute(getTarget().controls, "hidden", "hidden")));
    }
}
