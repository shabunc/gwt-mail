package org.shagen.mail.client.mappers;

import com.google.gwt.user.client.Window;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.Synchronizers;
import org.shagen.mail.client.models.HeaderProfile;
import org.shagen.mail.client.models.MailboxList;
import org.shagen.mail.client.views.HeaderProfileView;
import org.shagen.mail.client.views.MailboxListView;

import static jetbrains.jetpad.mapper.gwt.DomUtil.attribute;
import static jetbrains.jetpad.mapper.gwt.DomUtil.innerTextOf;

public class HeaderProfileMapper extends Mapper<HeaderProfile, HeaderProfileView> {

    public HeaderProfileMapper(HeaderProfile source) {
        super(source, new HeaderProfileView());
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        conf.add(Synchronizers.forPropsOneWay(getSource().userName, innerTextOf(getTarget().userName)));
        conf.add(Synchronizers.forPropsOneWay(getSource().userPic, attribute(getTarget().userPic, "src")));
    }
}
