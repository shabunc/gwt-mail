package org.shagen.mail.client.mappers;

import com.google.gwt.core.client.GWT;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.plugins.ajax.Ajax;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.Synchronizers;
import org.shagen.mail.client.gmail.FullMessageResponse;
import org.shagen.mail.client.gmail.MessageResponse;
import org.shagen.mail.client.gmail.MessagesResponse;
import org.shagen.mail.client.models.MessageItem;
import org.shagen.mail.client.views.MessageItemView;

import static com.google.gwt.query.client.GQuery.$;
import static jetbrains.jetpad.mapper.gwt.DomUtil.editableTextOf;
import static jetbrains.jetpad.mapper.gwt.DomUtil.hasClass;
import static jetbrains.jetpad.mapper.gwt.DomUtil.innerTextOf;

public class MessageItemMapper extends Mapper<MessageItem, MessageItemView> {
    MessageItemMapper(final MessageItem source) {
        super(source, new MessageItemView());


        $(getTarget().getElement()).click(new Function() {
            @Override
            public boolean f(Event e) {
                $(RootLayoutPanel.get().getElement()).trigger("MESSAGE_SELECTED", source);


                Ajax.get("/mail/get/" + source.id.get() , Ajax.createSettings()).done(new Function() {
                    @Override
                    public void f() {
//                        MessagesResponse messagesResponse = GWT.<MessagesResponse>create(MessagesResponse.class).load(arguments(0));
                        FullMessageResponse fullMessageResponse =  GWT.<FullMessageResponse>create(FullMessageResponse.class).load(arguments(0));
                        $(RootLayoutPanel.get().getElement()).trigger("MESSAGE_LOADED", fullMessageResponse);

                        source.isUnread.set(false);
                    }
                });

                return true;
            };
        });
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        conf.add(Synchronizers.forPropsOneWay(getSource().subject, innerTextOf(getTarget().subject)));
        conf.add(Synchronizers.forPropsOneWay(getSource().sender, innerTextOf(getTarget().sender)));
        conf.add(Synchronizers.forPropsOneWay(getSource().isUnread, hasClass(getTarget().getElement(), getTarget().stylesheet.isUnread())));
    }
}
