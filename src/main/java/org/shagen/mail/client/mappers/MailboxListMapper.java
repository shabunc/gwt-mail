package org.shagen.mail.client.mappers;

import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.mapper.MapperFactory;
import jetbrains.jetpad.mapper.gwt.WithElement;
import org.shagen.mail.client.models.Mailbox;
import org.shagen.mail.client.models.MessageItem;
import org.shagen.mail.client.models.MailboxList;
import org.shagen.mail.client.views.MailboxListView;

import static jetbrains.jetpad.mapper.Synchronizers.forObservableRole;
import static jetbrains.jetpad.mapper.gwt.DomUtil.withElementChildren;

public class MailboxListMapper extends Mapper<MailboxList, MailboxListView> {

    public MailboxListMapper(MailboxList source) {
        super(source, new MailboxListView());
    }

    @Override
    protected void registerSynchronizers(SynchronizersConfiguration conf) {
        super.registerSynchronizers(conf);

        // unfiltered list
        conf.add(forObservableRole(this, getSource().mailboxes, withElementChildren(getTarget().mailboxes),
                new MapperFactory<Mailbox, WithElement>() {
                    @Override
                    public Mapper<? extends Mailbox, ? extends WithElement> createMapper(Mailbox source) {
                        return new MailboxMapper(source);
                    }
                }));
    }
}
