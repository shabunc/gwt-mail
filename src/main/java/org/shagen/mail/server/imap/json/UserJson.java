package org.shagen.mail.server.imap.json;

import com.google.gson.annotations.Expose;

public class UserJson {

    @Expose
    private String userName;

    public UserJson(String userName) {
        this.userName = userName;
    }

}
