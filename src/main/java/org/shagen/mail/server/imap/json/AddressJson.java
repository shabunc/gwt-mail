package org.shagen.mail.server.imap.json;

import com.google.gson.annotations.Expose;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

public class AddressJson {

    @Expose
    private String email;

    @Expose
    private String personalName;

    public AddressJson(Address address) {
        InternetAddress internetAddress = (InternetAddress) address;
        this.email = internetAddress.getAddress();
        this.personalName = internetAddress.getPersonal();
    }

}
