package org.shagen.mail.server.imap.json;

import com.google.gson.annotations.Expose;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MessageJsonRecords {
    @Expose
    List<MessageJsonRecord> messages;

    @Expose  int total = 0;
    @Expose int from = -1;
    @Expose int to = -1;


    public MessageJsonRecords(Message[] messages, int total, int from, int to) throws IOException, MessagingException {
        this.messages = new ArrayList<MessageJsonRecord>();

        for (Message message : messages) {
            this.messages.add(new MessageJsonRecord(message));
        }

        this.total = total;
        this.from = from;
        this.to = to;
    }
}
