package org.shagen.mail.server.imap.json;

import com.google.gson.annotations.Expose;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;

import javax.mail.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MessageJsonRecord {
    @Expose
    private String subject;

    @Expose
    private String date;

    @Expose
    private List<String> from;

    @Expose
    private String htmlBody;

    @Expose
    private List<AddressJson> senders;
    private List<AddressJson> toRecipients;
    private List<AddressJson> ccRecipients;



    private void setHTMLBody(Message message) throws IOException, MessagingException {
//        Object content = message.getContent();
//
//        String body = null;
//        if (content instanceof String) {
//            body = (String) content;
//        } else if (content instanceof Multipart) {
//            System.out.println("=====>" + getText(message));
//            Multipart mp = (Multipart) content;
//            BodyPart bodyPart = mp.getBodyPart(1);
//
//
//            System.out.println("BODY PART:" + bodyPart.getContentType() + " => " + message.getSubject());
//            body = bodyPart.getContent().toString();
//        }

        this.htmlBody = getText(message);
    }


    /**
     * Return the primary text content of the message.
     */
    private String getText(Part p) throws
            MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            String s = (String)p.getContent();
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart)p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    if (s != null)
                        return s;
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart)p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }

        return null;
    }

    public MessageJsonRecord(Message msg) throws MessagingException, IOException {
        this.subject = msg.getSubject();


        this.from = new ArrayList<>(msg.getFrom().length);
        this.senders = new ArrayList<>(msg.getFrom().length);
        for (Address address : msg.getFrom()) {
            this.from.add(address.toString());
            this.senders.add(new AddressJson(address));
        }

        Address[] toRecipients =  msg.getRecipients(Message.RecipientType.TO);
        if (toRecipients != null) {
            this.toRecipients = new ArrayList<>(toRecipients.length);
            for (Address address : toRecipients) {
                this.toRecipients.add(new AddressJson(address));
            }
        }
        
        Address[] ccRecipients =  msg.getRecipients(Message.RecipientType.CC);
        if (ccRecipients != null ) {
            this.ccRecipients = new ArrayList<>(ccRecipients.length);
            for (Address address : ccRecipients) {
                this.ccRecipients.add(new AddressJson(address));
            }
        }

        // TODO (shabunc) use serializer;
        DateTime dateTime = new DateTime(msg.getSentDate());
        DateTime now = new DateTime();

        DateTimeZone zone = DateTimeZone.forID( "America/Los_Angeles" );
        int daysBetween = Days.daysBetween(dateTime, new DateTime()).getDays();

        if (daysBetween < 1) {
            this.date = dateTime.toString("hh:mm");
        } else {
            this.date = dateTime.toString("dd MMM");
        }


        this.setHTMLBody(msg);
    }
}
