package org.shagen.mail.server;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.gson.*;
import org.shagen.mail.server.gmail.*;
import org.shagen.mail.server.oauth.GmailClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GmailServlet {


    public void getMessage(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        String[] pathFragments = request.getRequestURI().substring(1).split("/");
        String messageId = pathFragments[pathFragments.length - 1];

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        GmailClient gmailClient = new GmailClient().withToken(token);

        Message messageApiResponse = gmailClient.getMessage(messageId);

        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Message.class, new MessageFullApiSerializer());
        Gson gson = gsonBuilder.disableHtmlEscaping().create();

        out.write(gson.toJson(messageApiResponse));
        out.close();
    }

    // TODO (shabunc): should think of a better solution
    private class Wrapper<T> {
        public T items;
        Wrapper(T content) {
            this.items = content;
        }
    }


    public void getProfile(HttpServletRequest request,
                            HttpServletResponse response) throws IOException {

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        GmailClient gmailClient = new GmailClient().withToken(token);

        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        Gson gson = new Gson();
        out.write(gson.toJson(gmailClient.getProfile()));
        out.close();
    }


    public void getLabels(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        GmailClient gmailClient = new GmailClient().withToken(token);

        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        Gson gson = new Gson();
        out.write(gson.toJson(gmailClient.getLabels()));
        out.close();
    }

    public void getMessages(HttpServletRequest request,
                     HttpServletResponse response) throws IOException {

        String[] pathFragments = request.getRequestURI().substring(1).split("/");
        String mailboxId = pathFragments[pathFragments.length - 1];

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        GmailClient gmailClient = new GmailClient().withToken(token);

        final List<Message> messages = gmailClient.getMessages("metadata", Collections.singletonList(mailboxId));
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Message.class, new MessageApiSerializer());
        Gson gson = gsonBuilder.create();

        out.write(gson.toJson(new Wrapper<List<Message>>(messages)));
        out.close();
    }

    public void getMailboxes(HttpServletRequest request,
                            HttpServletResponse response) throws IOException {

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        GmailClient gmailClient = new GmailClient().withToken(token);

        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Message.class, new MessageApiSerializer());
        Gson gson = gsonBuilder.create();

        out.write(gson.toJson(new Wrapper<List<Label>>(gmailClient.getLabelsData(Arrays.asList("INBOX", "SENT")))));
        out.close();
    }


}
