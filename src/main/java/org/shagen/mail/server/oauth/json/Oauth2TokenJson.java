package org.shagen.mail.server.oauth.json;


import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.gson.annotations.Expose;

public class Oauth2TokenJson {

    @Expose
    private String refreshToken;

    @Expose
    private String accessToken;

    @Expose
    private String scope;

    public Oauth2TokenJson(OAuth2AccessToken token) {
        refreshToken = token.getRefreshToken();
        accessToken = token.getAccessToken();
        scope = token.getScope();
    }
}
