package org.shagen.mail.server.oauth.json;

import com.google.gson.annotations.Expose;

public class ProfileEmailJson {

    public String value;

    public String type;

    public ProfileEmailJson() {}
}
