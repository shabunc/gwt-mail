package org.shagen.mail.server.oauth.json;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ProfileJson {

    public String displayName;
    public String id;

    public List<ProfileEmailJson> emails;


    public String getEmail() {
        if (emails == null) {
            return null;
        }

        return emails.get(0).value;
    };
}
