package org.shagen.mail.server.oauth;

import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.dev.util.collect.HashMap;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.shagen.mail.server.oauth.json.Oauth2TokenJson;
import org.shagen.mail.server.oauth.json.ProfileEmailJson;
import org.shagen.mail.server.oauth.json.ProfileJson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class GoogleServlet  extends RemoteServiceServlet {

    public void ping(HttpServletRequest request,
                     HttpServletResponse response) throws IOException {

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");


        response.setContentType("application/javascript");
        PrintWriter out = response.getWriter();

        Gson gson = new Gson();
        out.write(gson.toJson(new Oauth2TokenJson(token)));
        out.close();
    }

    public void validateCode(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        GoogleAuthClient googleAuthClient = new GoogleAuthClient();
        OAuth2AccessToken token = googleAuthClient.getAuthToken(request.getParameter("code"));

        GoogleCredential credential = new GoogleCredential().setAccessToken(token.getAccessToken());

        Plus plus = new Plus.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential).build();

        request.getSession(true).setAttribute("OAUTH2_TOKEN", token);

        Person profile = plus.people().get("me").execute();
            request.getSession(true).setAttribute("OAUTH2_PROFILE", profile);

        response.sendRedirect("/");
    }

    public void oauth2Login(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        GoogleAuthClient googleAuthClient = new GoogleAuthClient();

        final String authorizationUrl = googleAuthClient.getAuthUrl();

        response.sendRedirect(authorizationUrl);

        PrintWriter out = response.getWriter();
        out.write("{\"ok\": true}");
        out.close();
    }


    public void getProfile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Person profile = (Person) request.getSession().getAttribute("OAUTH2_PROFILE");

        response.setContentType("application/javascript");
        response.setCharacterEncoding("UTF-8");
        Gson gson = new Gson();

        PrintWriter out = response.getWriter();
        out.write(gson.toJson(profile));
        out.close();
        }
}
