package org.shagen.mail.server.oauth;

import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GoogleAuthClient {

    private OAuth20Service getOauth2Service() {
        final String clientId = "835714234681-ijl7fet5nelle1de3nt4otnb3oiie7lp.apps.googleusercontent.com";
        final String clientSecret = "XM20nOXp6xW0JVIM7p09v3W6";


        final OAuth20Service service = new ServiceBuilder()
                .apiKey(clientId)
                .apiSecret(clientSecret)
                .scope("https://mail.google.com https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile") // replace with
                // .state(secretState)
                .callback("http://127.0.0.1:8888/auth/oauth2/validate")
                .build(GoogleApi20.instance());

        // Replace these with your client id and secret
        final String secretState = "secret" + new Random().nextInt(999_999);

        return service;
    }


    public void refreshAccessToken(String refreshToken) throws IOException {
        OAuth20Service service = getOauth2Service();
        service.refreshAccessToken(refreshToken);
    }

    public String getAuthUrl() {
        // Replace these with your client id and secret
        final String secretState = "secret" + new Random().nextInt(999_999);

        // Obtain the Authorization URL
        System.out.println("Fetching the Authorization URL...");
        //pass access_type=offline to get refresh token
        //https://developers.google.com/identity/protocols/OAuth2WebServer#preparing-to-start-the-oauth-20-flow
        final Map<String, String> additionalParams = new HashMap<>();
        additionalParams.put("access_type", "offline");
        //force to reget refresh token (if usera are asked not the first time)
        additionalParams.put("prompt", "consent");

        OAuth20Service service = getOauth2Service();


        return service.getAuthorizationUrl(additionalParams);
    }


    public OAuth2AccessToken getAuthToken(String code) throws IOException {
        OAuth20Service service = getOauth2Service();
        return service.getAccessToken(code);
    }

    public Response makeSignedRequest(String requestUrl, OAuth2AccessToken token) {
        OAuth20Service service = getOauth2Service();
        OAuthRequest request = new OAuthRequest(Verb.GET, requestUrl, service);
        service.signRequest(token, request);
        return request.send();
    }

}
