package org.shagen.mail.server.oauth;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Response;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.*;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.shagen.mail.server.gmail.*;
import org.shagen.mail.server.oauth.json.Oauth2TokenJson;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GmailClient {

    private OAuth2AccessToken token;

    public GmailClient withToken(OAuth2AccessToken token) {
        this.token = token;
        return this;
    }

    private Gmail getService() {
        GoogleCredential credential = new GoogleCredential().setAccessToken(token.getAccessToken());
        Gmail gmail = new Gmail.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential).build();
        return gmail;
    }

    public Message getMessage(String messageId) throws IOException {
        markAsRead(messageId);
        return getService().users().messages().get("me", messageId).setFormat("full").execute();
    };


    public List<Message> getMessages(String format, List<String> labels) throws IOException {
        Gmail gmail = getService();
        ListMessagesResponse l = gmail.users().messages().list("me").setLabelIds(labels).execute();

        final List<Message> messages = new ArrayList<>();
        JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

            public void onSuccess(Message message, HttpHeaders responseHeaders) throws IOException {
                System.out.println(message.toPrettyString());
                messages.add(message);
            }

            public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
                System.out.println("Error Message: " + e.getMessage());
            }
        };

        BatchRequest batch = gmail.batch();
        for (Message message: l.getMessages()) {
            gmail.users().messages().get("me", message.getId()).setFormat(format).queue(batch, callback);
        }

        batch.execute();

        return messages;
    };


    public Profile getProfile() throws IOException {
        return getService().users().getProfile("me").execute();
    }

    public ListLabelsResponse getLabels() throws IOException {
        return getService().users().labels().list("me").execute();
    }


    public List<Label> getLabelsData(List<String> labelIds) throws IOException {
        Gmail gmail = getService();

        final List<Label> labels = new ArrayList<>();
        JsonBatchCallback<Label> callback = new JsonBatchCallback<Label>() {

            public void onSuccess(Label label, HttpHeaders responseHeaders) throws IOException {
                System.out.println(label.toPrettyString());
                labels.add(label);
            }

            public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
                System.out.println("Error Message: " + e.getMessage());
            }
        };


        BatchRequest batch = gmail.batch();
        for (String id: labelIds) {
            gmail.users().labels().get("me", id).queue(batch, callback);
        }

        batch.execute();
        
        return labels;
    }

    public Message markAsRead(String messageId) throws IOException {
        ModifyMessageRequest messageRequest = new ModifyMessageRequest()
                .setRemoveLabelIds(Collections.singletonList("UNREAD"));

        Message message = getService().users().messages().modify("me", messageId, messageRequest).execute();
        return message;
    }


    public Message markAsUnread(String messageId) throws IOException {
        ModifyMessageRequest messageRequest = new ModifyMessageRequest()
                .setAddLabelIds(Collections.singletonList("UNREAD"));

        Message message = getService().users().messages().modify("me", messageId, messageRequest).execute();
        return message;
    }

}
