package org.shagen.mail.server.gmail;

import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.io.BaseEncoding;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;

public class MessageFullApiSerializer implements JsonSerializer<Message> {

    @Override
    public JsonElement serialize(Message messageFullApiResponse, Type type, JsonSerializationContext context) {
        //JsonObject root = (JsonObject) context.serialize(messageFullApiResponse, MessageApiResponse.class);
        JsonObject root = new JsonObject();

        List<MessagePart> parts = messageFullApiResponse.getPayload().getParts();


        // naïve approach
        MessagePart htmlPart = Iterables.find(parts, new Predicate<MessagePart>() {
            @Override
            public boolean apply(@Nullable MessagePart message) {
                System.out.println("MIME :" + message);
                return message.getMimeType().equals("text/html");
            }
        });


        if (htmlPart != null) {
            String htmlContents = null;
            try {
                htmlContents = new String(Base64.getUrlDecoder().decode(htmlPart.getBody().getData()));
                root.addProperty("html", htmlContents);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return root;
    }
}
