package org.shagen.mail.server.gmail;

import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import javax.annotation.Nullable;
import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.lang.reflect.Type;
import java.util.List;

public class MessageApiSerializer implements JsonSerializer<Message> {

    private String getHeaderValueByName(final String name, List<MessagePartHeader> headers) {
        MessagePartHeader header = Iterables.find(headers, new Predicate<MessagePartHeader>() {
            @Override
            public boolean apply(@Nullable MessagePartHeader header) {
                return header.getName().equals(name);
            }
        });
        if (header != null) {
            return header.getValue();
        }
        return null;
    }

    @Override
    public JsonElement serialize(Message messageApiResponse, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();

        root.addProperty("id", messageApiResponse.getId());

        List<MessagePartHeader> headers = messageApiResponse.getPayload().getHeaders();

        root.addProperty("subject", getHeaderValueByName("Subject", headers));
        root.addProperty("date", getHeaderValueByName("Date", headers));


        String fromValue = getHeaderValueByName("From", headers);
        root.addProperty("from", fromValue);

        try {
            InternetAddress address = new InternetAddress(fromValue);
            root.addProperty("personalName", address.getPersonal());
            root.addProperty("address", address.getAddress());
        } catch (AddressException e) {

        }

        root.addProperty("isUnread", messageApiResponse.getLabelIds().contains("UNREAD"));

        return root;
    }
}
