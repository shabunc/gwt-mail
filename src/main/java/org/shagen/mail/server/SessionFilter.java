package org.shagen.mail.server;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.api.services.plus.model.Person;
import org.shagen.mail.server.oauth.GoogleAuthClient;
import org.shagen.mail.server.oauth.json.ProfileJson;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SessionFilter  implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = ((HttpServletRequest) servletRequest);
        final HttpServletResponse response = ((HttpServletResponse) servletResponse);

        OAuth2AccessToken token = (OAuth2AccessToken) request.getSession().getAttribute("OAUTH2_TOKEN");
        Person profile = (Person) request.getSession().getAttribute("OAUTH2_PROFILE");

        if ((token == null) || (profile == null)) {
            String authRedirectUrl = "/auth/login";
            System.out.println("redirecting to " + authRedirectUrl);

            response.sendRedirect(authRedirectUrl);

//            response.setContentType("application/javascript");
//            response.setCharacterEncoding("UTF-8");
//            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//            GoogleAuthClient googleAuthClient = new GoogleAuthClient();

//            PrintWriter out = response.getWriter();
//            out.println("{\"interrupted\": \"SessionFilter\",\"message\":\"token and/or profile data missing\"," +
//                    "\"redirectUrl\":\""+ googleAuthClient.getAuthUrl() +"\"," +
//                    " \"status\":\"" + String.valueOf(HttpServletResponse.SC_FORBIDDEN) + "\"}");
//            out.close();

        } else {
            filterChain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
}
